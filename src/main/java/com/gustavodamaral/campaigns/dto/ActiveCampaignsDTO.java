package com.gustavodamaral.campaigns.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor @NoArgsConstructor
public class ActiveCampaignsDTO {
    private List<CampaignResponseDTO> activeCampaigns;
}
