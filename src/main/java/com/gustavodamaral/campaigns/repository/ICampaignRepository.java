package com.gustavodamaral.campaigns.repository;

import com.gustavodamaral.campaigns.model.Campaign;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ICampaignRepository extends CrudRepository<Campaign, Long> {

    @Query("select c from Campaign c where c.effectiveDateEnd >= ?1")
    List<Campaign> findAllActive(LocalDate presentDate);

    @Query("select c from Campaign c where (c.effectiveDateStart between ?1 and ?2)" +
            "and (c.effectiveDateEnd between ?1 and ?2) order by c.effectiveDateEnd")
    Optional<List<Campaign>> findByPeriod(LocalDate startDate, LocalDate endDate);

    @Query("select c from Campaign c where c.effectiveDateEnd = ?1")
    Optional<List<Campaign>> findByEndDate(LocalDate endDate);

}
