package com.gustavodamaral.campaigns.service;

import com.gustavodamaral.campaigns.model.Campaign;
import com.gustavodamaral.campaigns.repository.ICampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CampaignService implements ICampaignService {

    @Autowired
    private ICampaignRepository campaignRepository;

    @Override
    public Campaign add(Campaign campaign) {

        Optional<List<Campaign>> samePeriod = campaignRepository.findByPeriod(campaign.getEffectiveDateStart(), campaign.getEffectiveDateEnd());

        Campaign campaignResponse = campaignRepository.save(campaign);

        samePeriod.ifPresent(campaigns -> campaigns.forEach(it -> {
            it.setEffectiveDateEnd(it.getEffectiveDateEnd().plusDays(1));
            update(it);
        }));

        return campaignResponse;
    }

    @Override
    public List<Campaign> listActives() {
        return campaignRepository.findAllActive(LocalDate.now());
    }

    @Override
    public void delete(Long id) {
        campaignRepository.deleteById(id);
    }

    @Override
    public Campaign update(Campaign campaign) {

        Optional<List<Campaign>> sameEndDate = campaignRepository.findByEndDate(campaign.getEffectiveDateEnd());

        sameEndDate.ifPresent(campaigns -> {
            if (campaign.getId() != campaigns.iterator().next().getId() && campaign.getEffectiveDateEnd().isEqual(campaigns.iterator().next().getEffectiveDateEnd())) {
                campaign.setEffectiveDateEnd(campaign.getEffectiveDateEnd().plusDays(1));
                update(campaign);
            }
        });

        campaign.setLastUpdateDate(LocalDateTime.now());
        return campaignRepository.save(campaign);
    }

    @Override
    public Campaign findById(Long id) {
        return campaignRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
