package com.gustavodamaral.campaigns.service;

import com.gustavodamaral.campaigns.model.Campaign;

import java.util.List;

public interface ICampaignService {

    Campaign add(Campaign campaign);
    List<Campaign> listActives();
    void delete(Long id);
    Campaign update(Campaign campaign);
    Campaign findById(Long id);

}
