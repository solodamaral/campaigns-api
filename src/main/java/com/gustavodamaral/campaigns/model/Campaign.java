package com.gustavodamaral.campaigns.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "TB_CAMPAIGN")
@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Builder
public class Campaign {

    @Id
    @GeneratedValue
    @Column(name = "ID_CAMPAIGN")
    private long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "ID_TEAM", nullable = false)
    private long teamId;

    @Column(name = "DT_EFFECTIVE_START", nullable = false)
    private LocalDate effectiveDateStart;

    @Column(name = "DT_EFFECTIVE_END", nullable = false)
    private LocalDate effectiveDateEnd;

    @Column(name = "DT_LAST_UPDATE")
    private LocalDateTime lastUpdateDate;

}
