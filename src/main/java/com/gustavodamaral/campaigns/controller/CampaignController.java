package com.gustavodamaral.campaigns.controller;

import com.gustavodamaral.campaigns.dto.ActiveCampaignsDTO;
import com.gustavodamaral.campaigns.dto.CampaignDTO;
import com.gustavodamaral.campaigns.dto.CampaignResponseDTO;
import com.gustavodamaral.campaigns.model.Campaign;
import com.gustavodamaral.campaigns.service.CampaignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.gustavodamaral.campaigns.constant.ResponseMessageConstant.CAMPAIGN_SUCCESSFULLY_REMOVED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(value = "Campaign")
@RestController
@RequestMapping(value = "/campaign")
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @ApiOperation(nickname = "addCampaign", value = "")
    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Campaign> add(@RequestBody @Validated final CampaignDTO campaignDto) {

        Campaign campaign = Campaign.builder()
                .name(campaignDto.getName())
                .teamId(campaignDto.getTeamId())
                .effectiveDateStart(campaignDto.getEffectiveDateStart())
                .effectiveDateEnd(campaignDto.getEffectiveDateEnd()).build();

        return ResponseEntity.ok(campaignService.add(campaign));
    }

    @ApiOperation(nickname = "updateCampaign", value = "")
    @PutMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Campaign> update(@RequestParam Long id, @RequestBody @Validated final CampaignDTO campaignDto) {

        Campaign campaign = Campaign.builder()
                .id(id)
                .name(campaignDto.getName())
                .teamId(campaignDto.getTeamId())
                .effectiveDateStart(campaignDto.getEffectiveDateStart())
                .effectiveDateEnd(campaignDto.getEffectiveDateEnd()).build();

        return ResponseEntity.ok(campaignService.update(campaign));
    }

    @ApiOperation(nickname = "getById", value = "")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Campaign> getById(@PathVariable Long id) {
        return ResponseEntity.ok(campaignService.findById(id));
    }

    @ApiOperation(nickname = "getActiveCampaigns", value = "")
    @GetMapping(value = "")
    public ResponseEntity<ActiveCampaignsDTO> getActiveCampaigns() {

        List<Campaign> campaigns = campaignService.listActives();
        List<CampaignResponseDTO> campaignsDto = new ArrayList<>();
        ActiveCampaignsDTO activeCampaigns = new ActiveCampaignsDTO();

        campaigns.forEach(it -> {
            CampaignResponseDTO dto = CampaignResponseDTO.builder()
                    .id(it.getId())
                    .name(it.getName())
                    .teamId(it.getTeamId())
                    .effectiveDateStart(it.getEffectiveDateStart())
                    .effectiveDateEnd(it.getEffectiveDateEnd())
                    .build();
            campaignsDto.add(dto);
        });

        activeCampaigns.setActiveCampaigns(campaignsDto);

        return ResponseEntity.ok(activeCampaigns);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        try {
            campaignService.delete(id);
            return ResponseEntity.ok(CAMPAIGN_SUCCESSFULLY_REMOVED);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
