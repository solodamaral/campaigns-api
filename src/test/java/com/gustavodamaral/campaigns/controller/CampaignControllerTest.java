package com.gustavodamaral.campaigns.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gustavodamaral.campaigns.dto.CampaignDTO;
import com.gustavodamaral.campaigns.model.Campaign;
import com.gustavodamaral.campaigns.service.CampaignService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.gustavodamaral.campaigns.constant.ResponseMessageConstant.CAMPAIGN_SUCCESSFULLY_REMOVED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = { ObjectMapper.class })
public class CampaignControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Mock
    private CampaignService service;

    @InjectMocks
    private CampaignController controller;
    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testAddCampaign() throws Exception {

        Campaign campaign = new Campaign();
        CampaignDTO campaignDto = CampaignDTO.builder().name("test").teamId((long) 1).effectiveDateStart(LocalDate.parse("2019-01-01")).effectiveDateEnd(LocalDate.parse("2019-01-03")).build();

        given(service.add(any())).willReturn(campaign);

        this.mockMvc
                .perform(
                        post("/campaign")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).writeValueAsString(campaignDto))
                                .characterEncoding("utf-8")
                )
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(campaign)));

    }

    @Test
    public void testUpdateCampaign() throws Exception {

        Campaign campaign = new Campaign();
        CampaignDTO campaignDto = CampaignDTO.builder().name("test").teamId((long) 1).effectiveDateStart(LocalDate.parse("2019-01-01")).effectiveDateEnd(LocalDate.parse("2019-01-03")).build();

        given(service.update(any())).willReturn(campaign);

        this.mockMvc
                .perform(
                        put("/campaign")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(mapper.registerModule(new JavaTimeModule()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).writeValueAsString(campaignDto))
                                .param("id", "1")
                                .characterEncoding("utf-8")
                )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(campaign)));

    }

    @Test
    public void testGetById() throws Exception{

        Campaign campaign = new Campaign();

        given(service.findById(any())).willReturn(campaign);

        this.mockMvc
                .perform(
                        get("/campaign/{id}", 1)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                )
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(campaign)));

    }

    @Test
    public void testGetActiveCampaigns() throws Exception{

        List<Campaign> campaigns = new ArrayList<>();
        List<CampaignDTO> dtos = new ArrayList<>();

        given(service.listActives()).willReturn(campaigns);

        this.mockMvc
                .perform(
                        get("/campaign")
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                )
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(dtos)));
    }

    @Test
    public void testDeleteCampaign() throws Exception{
        this.mockMvc
                .perform(
                        delete("/campaign/{id}", 1)
                                .contentType(MediaType.APPLICATION_JSON)
                                .characterEncoding("utf-8")
                )
                .andExpect(status().isOk())
                .andExpect(content().string(CAMPAIGN_SUCCESSFULLY_REMOVED));
    }

}
