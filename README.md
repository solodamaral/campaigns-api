# Campaigns API

### Overview
API de cadastro de campanhas desenvolvida em Java 8 utilizando Spring com testes em JUnit + Mockito, padronizada utilizando Model, Repository, Controller e Service Layers.
Documentação disponível em: http://localhost:8080/swagger-ui.html

### Info
Para iniciar aplicação é necessário possuir uma base de dados MySQL e configurar a conexão em [application.properties](https://bitbucket.org/solodamaral/campaigns-api/src/develop/src/main/resources/application.properties)

### Frameworks Utilizados
- Spring
- Swagger
- Lombok
- Jackson
- JUnit
- Mockito

### Banco de Dados
- MySQL
